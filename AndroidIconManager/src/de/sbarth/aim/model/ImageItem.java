package de.sbarth.aim.model;

import java.io.File;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

public class ImageItem {
    private final BooleanProperty isSelectedProperty = new SimpleBooleanProperty();
    private final File file;
    private final String filePath;

    public ImageItem(final String filePath) {
        this.file = new File(filePath);
        this.filePath = filePath;
    }

    public ImageItem(final File file) {
        this.file = file;
        this.filePath = file.getPath();
    }

    public File getFile() {
        return file;
    }

    public String getFilePath() {
        return filePath;
    }

    public boolean isSelected() {
        return isSelectedProperty.get();
    }

    public void setSelected(final boolean selected) {
        isSelectedProperty.set(selected);
    }

    public BooleanProperty isSelectedProperty() {
        return isSelectedProperty;
    }
}
